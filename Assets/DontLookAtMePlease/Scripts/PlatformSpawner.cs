using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    public GameObject platformToSpawn;
    public float spawnCooldown;
    public float xAxisOffset;

    private float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(platformToSpawn, new Vector3(0, 0, 30), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        if(timer >= spawnCooldown)
        {
            Instantiate(platformToSpawn, new Vector3(Random.Range(-xAxisOffset, xAxisOffset), 0, 30), Quaternion.identity);
            timer = 0f;
        }
        timer += Time.deltaTime;
    }
}
