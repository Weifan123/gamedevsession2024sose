using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this is a player controller
/// </summary>
public class PlayerController : MonoBehaviour
{
    public SpawningPlatforms spawner;
    public AnimationCurve jumpCurve;
    public AudioSource jumpSound;
    public AudioSource gameOverSound;

    PlatformMovement _targetPlatform;
    float _targetPlatformInitialZPosition;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            transform.position = new Vector3(((Input.mousePosition.x /Screen.width)-0.5f) * spawner.offset, 0, 0);
        }

        //if we have no target platform to jump to yet
        if (_targetPlatform == null)
        {
            //look through all existing platforms to find the closest one.
            var allPlatforms = FindObjectsByType<PlatformMovement>(FindObjectsSortMode.None);
            PlatformMovement closestPlatform = null;
            foreach (var platform in allPlatforms)
            {
                //if the platform is in front of us and we have no
                //closestPlatform yet or this platform is closer to us than the current closestPlatform  
                if (platform.transform.position.z > transform.position.z
                    && (closestPlatform == null
                    || platform.transform.position.z - transform.position.z < closestPlatform.transform.position.z - transform.position.z))
                {
                    //make this platform the closestPlatform for now.
                    closestPlatform = platform;
                }
            }

            //if we've found a platform after checking all platforms
            if (closestPlatform != null)
            {
                //assign the field so we can use it later on
                _targetPlatform = closestPlatform;
                //also remember how far we were from the platform for the jump animation
                _targetPlatformInitialZPosition = _targetPlatform.transform.position.z;
                jumpSound.Play();
            }
        }

        //if we have a targetPlatform, we are currently jumping
        if (_targetPlatform != null)
        {
            //value between 0 and 1 where 0 is the beginning, 0.5 the middle, 1 the end of the jump.
            // as the _targetPlatform moves closer to us, this value gets bigger.
            var jumpProgress = Mathf.InverseLerp(_targetPlatformInitialZPosition, transform.position.z, _targetPlatform.transform.position.z);

            //evaluate the jumpcurve to set the height of the player
            transform.position = new Vector3(transform.position.x, jumpCurve.Evaluate(jumpProgress), transform.position.z);

            //if the jump is completed
            if (jumpProgress == 1f)
            {
                //if there is no floor to stand on beneath us
                if (!Physics.Raycast(transform.position, Vector3.down))
                {
                    Destroy(gameObject);
                    gameOverSound.Play();
                }
                //reset variable so it'll get reassigned the next Frame when Update() is called again.
                _targetPlatform = null;
            }
        }
    }
}
