using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToLIve : MonoBehaviour
{
    public float deathtime = 15;

    private float timeElapsed;

    private void Update()
    {
        timeElapsed = timeElapsed + Time.deltaTime;

        if(timeElapsed > deathtime)
        {
            Destroy(gameObject);
            timeElapsed = 0;
        }
    }
}
