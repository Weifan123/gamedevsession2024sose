using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningPlatforms : MonoBehaviour
{
    public GameObject platformToSpawn;
    public float timer = 2f;
    public float offset;

    float timeUntilSpawn;

    // Start is called before the first frame update
    void Start()
    {
        timeUntilSpawn = timer;
    }

    // Update is called once per frame
    void Update()
    {
        timeUntilSpawn = timeUntilSpawn - Time.deltaTime;
        if (timeUntilSpawn <= 0)
        {
            var platform = Instantiate(platformToSpawn);
            platform.transform.position = transform.position + new Vector3(Random.Range(-offset * 0.5f, offset * 0.5f), 0, 0);
            timeUntilSpawn = timer;
        }
    }
}
